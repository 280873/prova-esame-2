run: main.out
	./main.out

clean:
	rm *.o*

main.out: grafi.o coda.o
	g++ compito.cc grafi.o coda.o -o main.out

grafi.o: grafi.cc
	g++ grafi.cc -c -o grafi.o

coda.o: coda.cc
	g++ coda.cc -c -o coda.o
