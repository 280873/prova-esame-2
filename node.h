/**
 * @file node.h
 * @author federica
 * @brief tipo di dato 'node'
 *
 * @copyright Copyright (c) 2021
 *
 */
struct node{
	char cont[81];
	char tipo;
};
